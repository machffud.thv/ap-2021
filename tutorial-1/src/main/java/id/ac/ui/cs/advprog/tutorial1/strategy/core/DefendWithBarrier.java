package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    public String defend(){
        return "Defend with barrier";
    }
    public String getType(){
        return  "Barrier";
    }
}
