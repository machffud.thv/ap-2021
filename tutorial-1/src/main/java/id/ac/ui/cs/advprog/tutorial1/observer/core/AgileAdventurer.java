package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild=guild;
        this.guild.add(this);
    }
    @Override
    public  void  update(){
        String tipeQuest = guild.getQuestType();
        if (tipeQuest.equalsIgnoreCase("R") || tipeQuest.equalsIgnoreCase("D")){
            this.getQuests().add(guild.getQuest());
        }
    }

}
