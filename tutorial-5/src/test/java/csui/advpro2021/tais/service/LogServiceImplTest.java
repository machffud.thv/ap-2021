package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cglib.core.CollectionUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;

    private Log log1;

    private Log log2;

    private LogSummary logSummary1;

    private LogSummary logSummary2;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa(
                "1906190606",
                "cbkadal",
                "cbkadal@cs.ui.ac.id", "4",
                "081312341234"
        );

        log1 = new Log();
        log1.setStart(LocalDateTime.parse("2021-04-06T05:00:00"));
        log1.setEnd(LocalDateTime.parse("2021-04-06T09:00:00"));
        log1.setMahasiswa(mahasiswa);
        log1.setDescription("Bantuan Pulsa Asisten Dosen");

        log2 = new Log();
        log2.setStart(LocalDateTime.parse("2020-04-06T11:00:00"));
        log2.setEnd(LocalDateTime.parse("2020-04-06T15:00:00"));
        log2.setMahasiswa(mahasiswa);
        log2.setDescription("Mengoreksi Pop Quiz");

        logSummary1 = new LogSummary(
                log1.getStart().getMonth().getValue(),
                log1.getStart().getYear(),
                log1.getJamKerja()
        );

        logSummary2 = new LogSummary(
                log2.getStart().getMonth().getValue(),
                log2.getStart().getYear(),
                log2.getJamKerja()
        );

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testServiceCreateLog() {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(logService.createLog(log1, mahasiswa.getNpm())).thenReturn(log1);
    }

    @Test
    public void testServicegetLogById() {
        lenient().when(logService.getLogById("0")).thenReturn(log1);
        Log resultLog = logService.getLogById("0");
        assertEquals(log1, resultLog);
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(log1, mahasiswa.getNpm());
        String currentLogDescription = log1.getDescription();
        log1.setDescription("Bantuan Kuota bulan April");

        lenient().when(logService.getLogById("0")).thenReturn(log1);
        lenient().when(logService.updateLog(log1, "0")).thenReturn(log1);
        Log resultLog = logService.getLogById("0");

        assertNotEquals(currentLogDescription, resultLog.getDescription());
        assertEquals(log1.getDescription(), resultLog.getDescription());
    }

    @Test
    public void testDeleteLog() {
        logService.createLog(log1, mahasiswa.getNpm());
        logService.deleteLog("0");
        lenient().when(logService.getLogById("0")).thenReturn(null);
        assertEquals(null, logService.getLogById("0"));
    }

    @Test
    public void testGetListLogsByNpmMahasiswa() {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Log> logs = logRepository.findByMahasiswa(mahasiswa);
        Iterable<Log> result = logService.getListLogsByNPMMahasiswa(mahasiswa.getNpm(), null);
        assertEquals(logs, result);
    }

    @Test
    public void testGetListLogsByNpmMahasiswaWithMonth() {
        List<Log> logs = new LinkedList<Log>(Arrays.asList(log1, log2));
        when(logRepository.findByMahasiswa(mahasiswa)).thenReturn(logs);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        Iterable<Log> result = logService.getListLogsByNPMMahasiswa(mahasiswa.getNpm(), "APRIL");
        logs.remove(log2);
        assertNotEquals(logs, result);
    }

    @Test
    public void testLogsSummaryByNPMMahasiswa() {
        List<LogSummary> summaries = Arrays.asList(logSummary1, logSummary2);
        when(logRepository.getRawLogSummaries(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogsSummaryByNPMMahasiswa(mahasiswa.getNpm(), null, null);
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testLogsSummaryByNPMMahasiswaWithMonth() {
        List<LogSummary> summaries = new LinkedList<>(Arrays.asList(logSummary1, logSummary2));
        when(logRepository.getRawLogSummaries(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogsSummaryByNPMMahasiswa(mahasiswa.getNpm(), "APRIL", null);
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testLogsSummaryByNPMMahasiswaWithYear() {
        List<LogSummary> summaries = new LinkedList<>(Arrays.asList(logSummary1, logSummary2));
        when(logRepository.getRawLogSummaries(mahasiswa.getNpm())).thenReturn(summaries);
        Iterable<LogSummary> summariesResult = logService.getLogsSummaryByNPMMahasiswa(mahasiswa.getNpm(), null, "2021");
        assertEquals(summaries, summariesResult);
    }

    @Test
    public void testGetMonthName() {
        assertEquals("APRIL", logService.getMonthName(log1.getStart()));
    }
}
