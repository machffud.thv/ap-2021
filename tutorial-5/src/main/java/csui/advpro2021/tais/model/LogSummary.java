package csui.advpro2021.tais.model;
import lombok.*;

import java.text.DateFormatSymbols;
import java.time.Duration;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogSummary {
    private String bulan;

    private int tahun;

    private double jamKerja;

    private double pembayaran;

    public LogSummary(int bulan, int tahun, double jamKerja) {
        this.bulan = new DateFormatSymbols().getMonths()[bulan-1];
        this.tahun = (int)(tahun);
        this.jamKerja = jamKerja;
        this.pembayaran = 350*jamKerja;
    }
}
