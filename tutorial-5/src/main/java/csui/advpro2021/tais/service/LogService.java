package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import java.util.List;

public interface LogService {

    public Log createLog(Log log, String npm);

    public Log getLogById(String id);

    public Log updateLog(Log log, String id);

    public void deleteLog(String id);

    public Iterable<Log> getListLogsByNPMMahasiswa(String npm, String month);

    public Iterable<LogSummary> getLogsSummaryByNPMMahasiswa(String npm, String month, String year);

}
