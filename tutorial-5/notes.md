#Requirements yang Didapat
##Entity MataKuliah
### Attribute
* kodeMatkul (key)
* nama
* prodi

### Hal yang dibutuhkan
* dapat membuat objek mata kuliah
* list semua mata kuliah
* dapat mengecek objek mata kuliah
* dapat mengakses objek mata kuliah dengan kodenya
* dapat mengubah kode mata kuliah
* hapus matakuliah

##Entity MataKuliah
### Attribute
* npm (key)
* nama
* email
* ipk
* noTelp
* kodeMatkul (FK ke MataKuliah)

### Hal yang dibutuhkan
* dapat membuat objek mahasiswa
* dapat menghapus objek mahasiswa
* dapat mengembalikan objek mahasiswa
* dapat mengubah kode npm
##Entity Log
### Attribute
* id (key)
* start_time
* end_time
* jamKerja
* description
* npmMahasiswa (FK ke mahasiswa)

### Hal yang dibutuhkan
* dapat membuat objek log
* dapat update log
* dapat menghapus log
* dapat mengembalikan semua log mahasiswa
* dapat mengembalikan log report


